import random

class chromosome (object):

    def __init__(self):

        self.genes = []
        self.fitness = 0

        for x in range(len(target)):
            if random.random() >= 0.5: self.genes.append(1)
            else: self.genes.append(0)

        for index in range(len(self.genes)):
            if self.genes[index] == target[index]: self.fitness += 1


    def get_genes(self): return self.genes

    def __str__(self): return str(self.genes)

class population (object):

    def __init__(self, size):
        self.size = size
        self.chromosomes = []

        for x in range(self.size): self.chromosomes.append(chromosome())

# Select Tournament Population
def select_tournament_population(pop):
        tournament_pop = population(0)
        for i in range(tournament_chromosome_size): tournament_pop.chromosomes.append(random.choice(pop.chromosomes))
        tournament_pop.chromosomes.sort( key = lambda x: x.fitness, reverse = True)
        return tournament_pop.chromosomes[0]

# Crossover Chromosomes
def crossover_chromosome(chromosome1, chromosome2):
    crossover_chrom = chromosome()
    for index in range(len(target)):
        if random.random() >= 0.5: crossover_chrom.genes[index] = chromosome1.genes[index]
        else: crossover_chrom.genes[index] = chromosome2.genes[index]

    return crossover_chrom

def evolve(pop):

    # Crossover Population
    crossover_population = population(0)
    for index in range(elite_chromosomes, population_size):
        chromosome1 = select_tournament_population(pop)
        chromosome2 = select_tournament_population(pop)
        crossover_population.chromosomes.append(crossover_chromosome(chromosome1, chromosome2))

    # Mutate Chromosome
    for index in range(elite_chromosomes, population_size-1):
        for chromosome in crossover_population.chromosomes:
            for index in range(len(target)):
                if random.random() < mutation_rate:
                    if random.random() < 0.5: chromosome.genes[index] = 1
                    else: chromosome.genes[index] = 0
    return pop

def main():

    global target, elite_chromosomes, population_size, tournament_chromosome_size, mutation_rate
    population_size = 3
    target = [1,0,1,0]
    mutation_rate = 0
    iterator = 1
    elite_chromosomes = 1
    tournament_chromosome_size = 4
    generation = 0
    Population = population(population_size)
    Chromosomes = Population.chromosomes

    # Sort the chromosomes according to their fitness.
    Chromosomes.sort( key = lambda x: x.fitness, reverse = True)

    print "-----------------------------------------------------------"
    print "Generation: ", generation, " | Fitness: ", Chromosomes[0].fitness
    print "-----------------------------------------------------------"

    for chromo in Chromosomes:
        print "Chromosome ", iterator, ": ", chromo, " | Fitness: ", \
        chromo.fitness, "\n"
        iterator += 1

    generation += 1

    while Chromosomes[0].fitness < len(target):
        Population = evolve(Population)
        #Chromosomes.sort( key = lambda x: x.fitness, reverse = True)
"""        print "-----------------------------------------------------------"
        print "Generation: ", generation, " | Fitness: ", Chromosomes[0].fitness
        print "-----------------------------------------------------------"

        for chromo in Chromosomes:
            print "Chromosome ", iterator, ": ", chromo, " | Fitness: ", \
            chromo.fitness, "\n"
            iterator += 1

            generation += 1
"""
if __name__ == "__main__": main()
